<?php 
	require __DIR__ . '/vendor/autoload.php';
	$smarty = new Smarty();
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Albondigas con tomate</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->

			
		<?php echo $smarty->display('menu.tpl') ?>
		

			
			
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
        <?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Albondigas con tomate</p>
						<h2>Albondigas con tomate</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<div class="receta whitebox">
																		<h1 itemprop="name">Albondigas con salsa de tomate</h1>
								 						<img src="https://i.blogs.es/b43ad5/albondigascontomate/1366_2000.jpg" align="middle">
	 						<div class="code-block code-block-4" style="margin: 8px 0; clear: both;">

<p></p>
<p><strong>INGREDIENTES</strong></p>
<p>500 g de carne picada (mezcla, pollo-pavo, para que queden más jugosas).</p>
<p>3 rebanadas de pan de molde sin corteza (unos 80 g aproximadamente) y un plato con leche entera para remojar la miga del pan</p>
<p>2 huevos</p>
<p>Sal, nuez moscada y pimienta negra recién molida (al gusto)</p>
<p>1 diente de ajo (si os gusta con más sabor 2)</p>
<p>1 plato de harina de trigo para pasar las albóndigas</p>
<p>Aceite de oliva virgen extra (para freír las albóndigas y las patatas fritas)</p>
<p>8 patatas grandes</p>
<p>Para la salsa de tomate: 2 latas grandes de tomate natural pelado (780 g x 2), aove, 1 diente y medio de ajo, sal, pimienta negra y una cucharada de azúcar.</p>

<hr>
<p><strong>ELABORACIÓN</strong></p>
<p>Cómo preparar albóndigas con salsa de tomate. Las albóndigas que os presento hoy son una receta de mi suegra Pilar, una de sus especialidades.</p>

<p>
Las medidas de esta receta son para 6 personas, me ha costado adaptarla porque la suya es para 15. Somos los que nos solemos juntar los sábados para comer en casa Gayo. Como a veces yo también llevo algún plato me estoy empezando a acostumbrar a cocinar en grandes cantidades y no es bueno, porque en casa somos dos y a este paso voy a tener que vender tuppers a domicilio.
</p>

<p>
Estas pequeñas delicias de esta receta de carne están increíbles pero es que además la salsa de tomate es espectacular, y la mezcla de las dos con las patatas fritas recién sacadas de la sartén… insuperables. Podría estar comiendo una detrás de otra hasta acabar la cazuela. Quedan muy suaves y saludables debido a la carne de ave casi sin grasa. Os aseguro que harán las delicias de muchos niños a los que no les gusta el sabor fuerte de la carne de ternera o cerdo.
</p>

<p>
Preparación de la salsa de tomate
</p>

<p>
Esta salsa que os voy a explicar no sólo es para esta receta, podéis emplearla para multitud de platos. Podéis cocinarla en grandes cantidades y la que os sobre la almacenáis en tarros y reserváis en la nevera.
Abrimos la lata de tomates enteros pelados y retiramos el exceso de líquido (el agua del tomate) ya que en este caso sólo vamos a utilizar la pulpa. El agua se puede guardar en un bote para otra futura receta o bien ponedla en un cazo y dejad reducir durante 15 minutos para tener pasta de tomate concentrada.
Introducimos en una cazuela unas 5 cucharadas de aceite y mientras se calienta pelamos los ajos y fileteamos finamente. Añadimos a la cazuela los ajos fileteados, los doramos y cuando comiencen a tener color introducimos los tomates y los partimos a la mitad con ayuda de una cuchara de madera.
Dejamos pochando hasta que reduzcan a menos de la mitad de su tamaño, unos 30 minutos a fuego lento. Si tenéis prisa podéis hacerlo en 15-20 minutos a fuego medio-alto removiendo continuamente para que no se pegue el tomate a la cazuela (pero el sabor no será el mismo).
Añadimos una cucharadita de azúcar para reducir acidez, una pizca de sal y pimienta negra recién molida. Si os gusta el orégano, tomillo o albahaca podéis tunear vuestra salsa. Removemos todo bien juntando sabores.
Probamos y rectificamos con sal, aunque lo más seguro es que no haga falta. Removemos unos minutos, apartamos del fuego y terminamos pasando la salsa por el chino. Así nos quedará más fina, aunque si os gusta encontraros pedacitos de tomate (a mi me encanta), obviad este paso. Reservamos para juntar con las albóndigas.
</p>

<p>
Preparación de las albóndigas
</p>

<p>
Lo primero de todo es hacer las pelotas de carne, que todas salgan más o menos igual. Salpimentamos la carne picada, tanto la de pollo como la pavo en un cuenco grande. Rompemos los 2 huevos y los añadimos junto con la nuez moscada. Las rebanadas de pan sin la corteza que previamente hemos remojado en leche durante unos minutos. Y por último el ajo machacado o muy picadito (sin el brote interior o tronco para que no repita).
Removemos todo bien con las manos hasta que se mezclen los ingredientes. Esta será la base de nuestras futuras albóndigas.
Sin miedo empezamos a trabajar la masa haciendo pequeñas (o grandes, eso al gusto) pelotas que luego pasaremos por harina. Las dejamos en un plato a la espera de la sartén.
Las sacudimos un poco para retirar el exceso de harina y freímos en aceite de oliva virgen extra bien caliente. Unos tres minutos son suficientes y reservamos.
Pasamos las albóndigas a la cazuela con la salsa de tomate recién hecha. Calentamos a fuego medio hasta que quede una salsa de tomate ligeramente espesa (por la harina) durante unos 10 minutos.
Dejamos que reposen unos 5 minutos mientras freímos las patatas con el tipo de corte que más te guste.</p>
</p>
 
						</div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>


