<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
<?php

require __DIR__ . '/vendor/autoload.php';
$smarty = New Smarty();

echo $smarty->display('menu.tpl');

?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>La mejor comida Riojana</p>
						<h2>Chuletillas al Sarmiento</h2>
					</header>
				</div>
			</section>



		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
					
									<div class="inner">
									<br/>
				<p><center><a href="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg"><img data-attachment-id="107" data-permalink="https://elojaesmenesiano.wordpress.com/2014/11/25/recetas-tipicas-de-ezcaray-chuletas-al-sarmiento/chuletillas/#main" data-orig-file="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg" data-orig-size="1000,666" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="CHULETILLAS" data-image-description="" data-medium-file="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=300" data-large-file="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=665" class="alignnone size-full wp-image-107" src="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=665&#038;h=442" alt="CHULETILLAS" width="665" height="442" srcset="https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=665&amp;h=442 665w, https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=150&amp;h=100 150w, https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=300&amp;h=200 300w, https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg?w=768&amp;h=511 768w, https://elojaesmenesiano.files.wordpress.com/2014/11/chuletillas.jpg 1000w" sizes="(max-width: 665px) 100vw, 665px" /></a></center></p>
			</div>
					
						<div class="content">
							<header class="align-center">
								
								<h2>Ingredientes para las chuletillas al sarmiento para 6 personas:</h2>
							</header>

<ul></p>
		<li>1 kilo y medio de chuletillas de cordero</li>
		<li>1 gavilla de sarmientos</li>
		<li>Aceite</li>
		<li>Sal</p></li>
</ul>	</div>
					</div>
				</div>
			</section>
		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<header class="align-center">
								
								<h2>Preparación</h2>
							</header>
							<p>Se prepara fuego con la gavilla de sarmientos y se coloca encima de la parrilla tras untarla de aceite. Se deja quemar para desinfectarla y poder tener limpieza a la hora de cocinar.
		<p><a href="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg"><center><img data-attachment-id="102" data-permalink="https://elojaesmenesiano.wordpress.com/2014/11/25/recetas-tipicas-de-ezcaray-chuletas-al-sarmiento/attachment/12/#main" data-orig-file="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg" data-orig-size="1200,797" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="12" data-image-description="" data-medium-file="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=300" data-large-file="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=665" class="alignnone size-full wp-image-102" src="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=665&#038;h=441" alt="12" width="665" height="441" srcset="https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=665&amp;h=441 665w, https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=150&amp;h=100 150w, https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=300&amp;h=199 300w, https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=768&amp;h=510 768w, https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg?w=1024&amp;h=680 1024w, https://elojaesmenesiano.files.wordpress.com/2014/11/12.jpg 1200w" sizes="(max-width: 665px) 100vw, 665px" /></a></center><p>
		Despues, se limpia con papel de periodico para eliminar los posibles restos de comida o impurezas de otros días.
		Al conseguir unas buenas brasas, ya sin llama, colocamos las chuletillas sobre la parrilla.
		Cuando estén hechas por ambos lados, se sazonan con sal y ya están listas para degustar.
		Es una receta, muy fácil de elaborar y, además, realmente típica de nuestra zona.
		Entre las palabras utilizadas encontramos algunas muy típicas de La Rioja que, cuando se pronuncian en otras comunidades, son difíciles de reconocer. Por ejemplo; gavilla (conjunto de sarmientos, cañas, mieses, ramas, etc., mayor que el manojo y menor que el haz).
		<p>Hay que concretar que las Chuletillas al sarmiento es un plato típico de la totalidad de la Comunidad.</p>
</p>
						</div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>