<?php 
	require __DIR__ . '/vendor/autoload.php';
	$smarty = new Smarty();
?>

<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
    <?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Simples y muy ricos</p>
						<h2>Huevos fritos</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<header class="align-center">
								<h2>La receta</h2>
              </header>
              <p style="text-align: center">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Three_fried_eggs.jpg/800px-Three_fried_eggs.jpg" alt="">
              </p>
              <p>El huevo está compuesto de dos partes fácilmente identificables, el centro o yema, de color amarillo (que debe estar líquida en el huevo frito), y la parte blanca que la rodea, la clara (que debe estar sólida con los extremos crujientes). Estas dos partes se preparan juntas, y es habilidad del cocinero mantener los tiempos de fritura bien vigilados (por regla general entre los dos o tres minutos) para poder hacer un buen huevo frito con la textura y los aromas apropiados. Para ello basta con seguir los siguientes consejos:</p> 

              <p> Hacer los huevos fritos con huevos a temperatura ambiente (10 °C a 20 °C) y estables previamente durante varias horas, en ningún caso recién sacados del refrigerador (este es un error frecuente).</p>

              <p>Emplear huevos frescos, los que se han conservado van perdiendo la humedad interior a través de sus poros y dejan un producto final no tan apetecible. Los huevos frescos sufren menos roturas de las yemas cuando se elaboran.</p>

              <p>
                Tener bien caliente el aceite, listo para freír un huevo, que se suele romper directamente sobre la sartén. Por esta razón es mejor emplear aceites con alto punto de humo: como el aceite de oliva (180 °C). La temperatura ideal del aceite es a 120 °C.1​ A mayor temperatura se pierde la textura tierna pero se gana en sabor.
              </p>

              <p>Realizar uno o dos huevos fritos cada vez, dependiendo de la superficie del fondo de la sartén, a mayor superficie mayor número de huevos fritos simultáneos. Si se deben freír varios en poco tiempo lo mejor es emplear diversas sartenes.</p> 
              <p>No verter la sal hasta haber terminado de freír completamente el huevo en la sartén.2​ Generalmente lo hace cada comensal o se hace fuera de la sartén (en ningún caso durante la fritura del huevo).</p>
              <p> Servir caliente y recién frito a los comensales, no conviene esperar ni un minuto, ya que el huevo frito queda de otra forma aceitoso y la yema empieza a cuajar.</p>
              <p>Emplear una espumadera para recoger el huevo frito de la sartén o poder darle la vuelta (operación esta poco frecuente cuando se trata de huevos fritos).</p>
              <p>Para evitar que al freír el huevo el aceite salpique en exceso hay un procedimiento sencillo y que no altera el gusto ni el aspecto del huevo: cuando el aceite está suficientemente caliente espolvorear un poco de harina y seguidamente echar el huevo en la sartén.						</p>

              </div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>