<?php 
	require __DIR__ . '/vendor/autoload.php';
	$smarty = new Smarty();
?>

<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
        <?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Aguita Arreglada en un hotfix.....</p>
						<h2>Y para beber...Cambio esto</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
		
                            <div style="height:40px"></div>
                            <div style="float:left;width:40%">
                                <h2>Ingredientes</h2>
                                <ul>
                                    <li>Una medida de hidrógeno</li>
                                    <li>Dos medidas de oxigeno</li>
                                    <li>Un vaso</li>
                                </ul>
                                <h2>Preparación</h2>
                                <p>Ir a la cocina, trincar un vaso del armario.</p>
                                <p>Con la parte abierta del vaso apuntando hacia arriba, se abre cuidadosamente el grifo.</p>
                                <p>Dejar correr unos instantes para evitar beberte el agua caliente de fregar.</p>
                                <p>Posicionar el vaso bajo el chorro, y mantenerlo hasta que el agua llegue a un dedo del borde.</P>
                                <p></p>
                            </div>
                            <div style="float:right;width:60%;text-align:right">
                                <img src="images/aguita.jpg" style="width:90%" />
                            </div>
                            <div style="clear:both"></div>
						</div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
