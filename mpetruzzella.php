<?php

require __DIR__ . '/vendor/autoload.php';
$smarty = New Smarty();
?>
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<?php echo $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Para el aperitivo</p>
						<h2>Carta de cervezas Y picoteo</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<p><img src="https://www.expohorecamagazine.com/uploads/fotos_noticias/2017/03/w800px_15047-122614-la-carta-de-cervezas-una-propuesta-en-auge-que-se-cuela-en-los-bares-y-restaurantes.jpg" /></p>
							<p>Vivamus nec odio ac ligula congue feugiat at vitae leo. Aenean sem justo, finibus sed dui eu, accumsan facilisis dolor. Fusce quis dui eget odio iaculis aliquam vel sed velit. Nulla pellentesque posuere semper. Nulla eu sagittis lorem, a auctor nulla. Sed ac condimentum orci, ac varius ante. Nunc blandit quam sit amet sollicitudin sodales.</p>
							<p><img width="400" src="https://s3.eestatic.com/2015/02/15/cocinillas/Cocinillas_11258928_115867835_1706x960.jpg"></p>
<p style="font-size:40px;font-weight:bold;">A mi amigo Alfonso, que siempre me anima para mejorar mis recetas.</p>
<h3>Ingredientes para hacer patatas bravas paso a paso con su salsa brava casera (4 personas):</h3>
<ul><li>4 patatas de tamaño medio, suele ser 1 por persona aunque puedes preparar la cantidad que prefieras.</li>
<li>Abundante aceite para freír las patatas.</li><li>
Sal al gusto.</li></ul>
<h3>Para la salsa brava</h3><ul><li>
2 cucharadas de postre de pimentón (puedes poner las dos picantes, las dos dulces o bien una de cada, a tu gusto).</li><li>
1 cebolla mediana.</li><li>
250 gr de agua.</li><li>
20 gr de harina.</li><li>
2 cucharadas de aceite de oliva.</li><li>
1/2 pastilla de caldo de carne concentrado.</li><li>
1 diente de ajo.</li><li>
Sal.</li></ul>
<h3>En algunos lugares se sirven además con salsa alioli casera, estos son los ingredientes que necesitarás:</h3>
<ul><li>1 huevo</li><li>
1/2 vaso (100 ml) de aceite de oliva y 1/2 vaso de aceite de girasol. Si quieres hacer más cantidad de ajo, simplemente aumenta la cantidad de aceite y dientes de ajo, manteniendo el resto de ingredientes.</li><li>
1-2 dientes de ajo (según si lo prefieres más fuerte o suave).</li><li>
Una pizca de sal.</li><li>
El zumo de 1/2 limón.</li></ul>
						</div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>