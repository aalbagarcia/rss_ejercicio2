<?php 
	require __DIR__ . '/vendor/autoload.php';
    $smarty = new Smarty();
?>

<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Para los mas golosos</p>
						<h2>brownies de chocolate</h2>
					</header>
				</div>
			</section>


			<div name="miimagen"><img width="525" height="360" src="https://www.recetasderechupete.com/wp-content/uploads/2010/01/brownie_clasico.jpg" class="mainphoto wp-post-image" alt="Brownies de chocolate" srcset="https://www.recetasderechupete.com/wp-content/uploads/2010/01/brownie_clasico.jpg 525w, https://www.recetasderechupete.com/wp-content/uploads/2010/01/brownie_clasico-150x103.jpg 150w" sizes="(max-width: 525px) 100vw, 525px" style="margin-left: 500px;></div>
			
			<br/><br/>

		<!-- Two -->
			<h3>Preparación los brownies de chocolate</h3>
			<ol>
			<li>Lo primero es preparar el recipiente donde los vamos a cocinar. He utilizado una bandeja&nbsp;de 20 x 20cm de cristal y papel de horno.</li>
			<li>La medida de la bandeja da para 9 brownies generosos, pero todo depende de como queramos que sea de grueso. Si los quer de menor tamaño emplead una bandeja mayor.</li>
			<li>Forramos la bandeja y la untamos con mantequilla. Espolvoreamos con chocolate en polvo el papel de horno. No he utilizado harina porque la última vez que lo prepare quedaron pequeñas motas de harina blanca en la base.</li>
			<li>Con este truco solucionamos este problema y encima le damos más sabor a chocolate a este pastelillo.</li>
			<li>Echamos&nbsp;en un cuenco el chocolate rallado o en trozos pequeños con la mantequilla y lo ponemos en el microondas durante&nbsp;4 minutos a temperatura media. Sacamos y juntamos bien la mezcla con una varilla hasta que quede una crema parecida a <a title="Crema de chocolate y avellanas Nocilla Nutella" href="//www.recetasderechupete.com/crema-de-cacao-y-avellanas-nocilla-o-nutella-casera/6011/"  rel="noopener noreferrer"></li>
			</ol>
			
			
			<div itemscope itemtype="https://schema.org/VideoObject"><iframe width="738" height="415" src="https://www.youtube.com/embed/QSB944opcns?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><meta itemprop="name" content="Como hacer brownie de chocolate"><meta itemprop="description" content="En este breve y sencillo video podrá ver Receta brownie de chocolate"><link itemprop="thumbnailUrl" href="https://i.ytimg.com/vi/QSB944opcns/hqdefault.jpg"/><meta itemprop="uploadDate" content="2018-11-16T09:11:04+0000"/></div>
			
			
			<h3>Preparacióe la crema&nbsp;de los brownies</h3>
			<ol>
			<li>Batimos en un bol grande los huevos con el azúcar hasta que quede una crema espumosa. A񡤩mos la cucharada de vainilla y el bicarbonato. Vertemos la crema&nbsp;del chocolate en el bol anterior y mezclamos&nbsp;bien con las varillas.</li>
			<li>Tamizamos la harina con un colador y añadimos a la anterior mezcla. Revolvemos muy bien hasta conseguir una pasta crema homoga.</li>
			<li>Vertemos la crema&nbsp;de chocolate&nbsp;en la bandeja de cristal. Os aconsejo que lo hagᩳ con una espᴵla de silicona para aprovechar toda la crema y que no quede nada en el bol.</li>
			<li>Partimos las nueces en trocitos y las pasamos por un poco de&nbsp;chocolate en polvo. Las introducimos en la crema y con un tenedor las cubrimos bien con el chocolate.</li>
			</ol>
			<div itemscope itemtype="https://schema.org/VideoObject"><iframe width="738" height="415" src="https://www.youtube.com/embed/ve-9ucygWwo?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><meta itemprop="name" content="Receta de Brownie de chocolate"><meta itemprop="description" content="En este breve y sencillo video podrá ver Receta de Brownie de chocolate"><link itemprop="thumbnailUrl" href="https://i.ytimg.com/vi/ve-9ucygWwo/hqdefault.jpg"/><meta itemprop="uploadDate" content="2018-11-16T09:11:04+0000"/></div>
			<h3>Preparación horneado final de los brownies</h3>
			<ol>
			<li>Horneamos&nbsp;el brownie en el horno precalentado a 180º C en la parte intermedia durante 35-40&nbsp;minutos con el horno caliente arriba y abajo (sin aire).</li>
			<li>Tal como comenté alguna otra receta, todo depende del horno, siempre mirad con un tenedor que al pinchar salga seco.</li>
			<li>Cuando lleve ya 20 minutos en el horno tapamos con un poco de papel de aluminio para que no se queme la superficie y la costra quede perfecta y crujiente.</li>
			<li>En mi horno el tiempo de preparacion 40 minutos exactos pero en el de mi madre con 30-35 llega. No os debe pasar con el&nbsp;tiempo porque os saldrá unos pastelillos secos que no hay quien se los coma.</li>
			<li>Lo dejamos enfriar en la bandeja y cuando esté cortamos en varias porciones. Los separamos del papel de horno&nbsp;y presentamos en un plato con un poco de <a title="Helado de chocolate" href="//www.recetasderechupete.com/helado-de-chocolate-casero-como-preparar-el-helado-mas-cremoso/6074/"  rel="noopener noreferrer"><strong>helado de vainilla o chocolate</strong></a>, o con chocolate caliente. Para los paladares chocolateros más exigentes.</li>
			</ol>



		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>