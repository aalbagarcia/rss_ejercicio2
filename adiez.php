<?php 
	require __DIR__ . '/vendor/autoload.php';
	$smarty = new Smarty();
?>

<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
        <?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>macarroncitosssss mmmmm...............</p>
						<h2>Macarrones al queso estilo americano</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<div class="receta whitebox">
								
								<div class="code-block code-block-4" style="margin: 8px 0; clear: both;">


									<p>400 gr de macarrones rizados finos</p>
									<p>1 cucharada rasa de harina</p>
									<p>Una cucharada colmada de mantequilla</p>
									<p>1 cucharadita de postre de mostaza suave</p>
									<p>180&nbsp;gr de queso rallado de sabor fuerte (el mejor es el Cheddar)</p>
									<p>1 vaso (250 ml) de leche</p>
									<p>Sal y pimienta</p>
<img src="https://www.divinacocina.es/wp-content/uploads/mACARRONES-QUESO-AMERICANOS-2-570x570.jpg">
									<hr>
									<p><strong>ELABORACIÓN</strong></p>
									<p><strong>Hervimos</strong> los macarrones según las instrucciones del envase. Intenta que la pasta quede entera y en su punto.</p>
									<p>En una sartén al fuego ponemos la <strong>mantequilla</strong> hasta que se derrita. Antes de que llegue a quemarse añadimos la harina y removemos para que tome color.</p>
									<p>Ponemos la cucharadita de <strong>mostaza</strong> y vamos añadiendo poco a poco la leche removiendo sin parar para que no se formen grumos. Salpimentamos.</p>
									<p>Cuando la <strong>salsa</strong> esté bien ligada y en su punto añadimos 150 gr de queso (reservamos un poco para gratinar) y mezclamos bien.</p>
									<p>Escurrimos los <strong>macarrones</strong> del agua de cocción pero sin enjuagarlos y cuando están bien calientes mezclamos con la salsa de queso con movimientos fuertes para que ligue todo bien.</p>
									<p>Ponemos en un recipiente de <strong>horno</strong>, cubrimos con el resto del queso y damos un golpe bajo el grill hasta que la superficie dore un poquito. <strong>Servimos</strong> al momento.</p>

		 
								</div>
							</div>
						</div>
			</section>

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
