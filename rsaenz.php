<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
<?php

require __DIR__ . '/vendor/autoload.php';
$smarty = New Smarty();

echo $smarty->display('menu.tpl');

?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>un clásico de madrugada despues de la rumba</p>
						<h2>Reina Pepiada</h2>
					</header>
				</div>


			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<header class="align-center">
								
								<h2>Ingredientes para Arepas venezolanas Reina Pepiada</h2>
							</header>
<div style="float:left;width:40%">
<ul><li>2 tazas de harina de maíz precocida P.A.N. o similar (blanca)</li><li>
    4 tazas de agua tibia</li><li>
    1 cucharada de postre de sal</li><li>
    Para el relleno “La Reina pepiada”: 400 g. de pechuga de pollo</li><li>
    2 dientes de ajo</li><li>
    1 cebolla mediana</li><li>
    1 aguacate maduro</li><li>
    3 cucharadas de mayonesa casera</li><li>
    25 ml. de aceite de oliva virgen extra</li><li>
    Sal y pimienta negra recién molida (al gusto)</li>
</ul>	</div>
                            <div style="float:right;width:60%;text-align:right">
                                <img src="https://images.kitchenstories.io/communityImages/b860c669d5c82a842432d9dd4289666c_4b1dd878-431a-40d9-9a13-9041b37c58ca/b860c669d5c82a842432d9dd4289666c_4b1dd878-431a-40d9-9a13-9041b37c58ca-large-landscape-150.jpg" style="width:90%" />
                            </div>
                            <div style="clear:both"></div>
														<header class="align-center">
								
								<h2>Preparación</h2>
							</header>
							<p>Las arepas se elaboran con harina de maíz ya precocida, que puede ser blanca o amarilla.

En España la más habitual en cualquier gran superficie es la marca P.A.N., la cual es perfecta ya que es también muy usada en Venezuela y el resultado final es satisfactorio.

    Echamos el agua en un bol grande y salamos. Añadimos la harina y comenzamos a remover con la ayuda de una cuchara o tenedor.
    Al utilizar agua tibia, conseguimos que la harina se integre mejor y no se produzcan grumos.
    En cuanto vemos que coge consistencia la mezcla, pasamos a trabajar con las manos.
    Vamos mezclando/amasando en movimientos de fuera hacia dentro, de manera que la harina absorba completamente el agua y nos resulte una masa uniforme y compacta.
    Sabemos que la masa está en su punto óptimo cuando no se pegan a las manos.</p>
	
	<p>Preparación y forma de las arepas

    Lavamos las manos y vamos haciendo bolas del tamaño un poco mayor que una nuez (como si hiciésemos albóndigas) y las aplastamos hasta conseguir una forma redondeada.
    Como vamos a rellenarlas, las haremos de un dedo de grosor (1,5 cm) y entre 8-10 cm de diámetro.
    Si vemos que se nos ha formado alguna grieta en la masa, le damos con un poco de agua y la cerramos.

Arepas a la plancha. Una forma de hacerlas

    En una sartén bien caliente (haremos de 4 en 4), echamos un poco de aceite (para que luego no se nos peguen).
    Calentamos a temperatura media para que las arepas se vayan cocinando bien y no resulten crudas por dentro.
    Las cocinamos vuelta y vuelta, aproximadamente unos 14-20 minutos.
    Tenemos que ir mirando que adquieran cierto color dorado/tostado por el exterior. No hay prisa y veréis como se van haciendo a vuestro gusto.
    Las que veáis que ya están en su punto las ponemos en una bandeja en el horno a 100º C para que no pierdan temperatura. Así mantendrán el crujiente exterior.
    Vamos repitiendo el proceso con el resto y reservamos en una fuente cuando estén todas hechas.
    Las tapamos con un paño para que no enfríen o hacemos el truqui del horno que os comentaba anteriormente.

Preparación del relleno “La reina pepiada” de nuestras arepas

    Cortamos la carne de pollo en pequeños trozos y la salpimentamos al gusto.
    Podemos usar la que más nos guste como pechuga, contramuslo, muslo, etc…
    En un sartén con un poco de aceite, vamos pochando la cebolla, el ajo, y más tarde añadimos la carne de pollo.
    Salteamos hasta que nos quede jugosa y reservamos todo en un bol.
    Pelamos el aguacate, quedándonos con la carne más madura del interior.
    Lo cortamos en trocitos pequeños, lo añadimos al bol y mezclamos bien todos los ingredientes.
    Al estar maduro el calor lo ablandará y se mezclará fácilmente con el resto de ingredientes.
    Añadimos ahora 3 cucharadas de mayonesa y mezclamos. ¡Y ya tenemos listo nuestro delicioso relleno!
</p>
					</div>
				</div>
			</section>
		

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>