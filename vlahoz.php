<?php
require __DIR__ .'/vendor/autoload.php';
$smarty = new Smarty();
?>

<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
		<?php $smarty->display('menu.tpl'); ?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Receta de...</p>
						<h2>Coles de bruselas con ajo y guindilla</h2>
					</header>
				</div>
			</section>

		<!-- Two -->
		
<div class="entry-content clearfix">
			<!-- post-start --><div id="HOTWordsTxt"><div id="MKW25345"><p><a href="https://gastronomiaycia.republica.com/fotos/recetas/colbruselas_pinon_guindilla.jpg" title="Coles de Bruselas con piñones, ajo y guindilla"><img src="https://gastronomiaycia.republica.com/wp-content/photos/colbruselas_pinon_guindilla.jpg" class="centro" alt="Coles de Bruselas con piñones, ajo y guindilla" width="680" height="491"></a></p>
<p>Esta receta de <strong>Coles de Bruselas con piñones, ajo y guindilla</strong> es muy sencilla, pero una buena baza para animar a quienes no son muy amigos de esta crucífera, a nosotros nos ha dado muy buen resultado siempre (empiezas poniendo dos mitades a quien está enemistado con este alimento y acaba cogiendo de tu plato), aunque también hay que valorar que agrade el picante que aporta la guindilla a este plato, hay que moderar su uso y en caso de no gustar se puede omitir.</p>
<p>La elaboración de esta <strong>receta</strong> de <strong>Coles de Bruselas</strong> es rápida y fácil, y si sois de los que disfrutan de estas pequeñas coles y habitualmente las consumís hervidas o cocidas al vapor, vas a disfrutar el doble con esta receta, sea como entrante o como guarnición de una carne o pescado. Probadla y nos comentáis que os parecen. Recordad que podéis saber un poco más sobre esta verdura en el post <a href="https://gastronomiaycia.republica.com/2010/10/26/col-de-bruselas/">Col de Bruselas</a> y vamos a por la receta, que es ideal para esta temporada.<br>
<span id="more-44860"></span></p>
<h3>Ingredientes (4 comensales)</h3>
<ul><li><span itemprop="ingredients">1 kilo de coles de Bruselas frescas</span></li><li><span itemprop="ingredients">4 c/s de piñones</span></li><li><span itemprop="ingredients">2 dientes de ajo grandes</span></li><li><span itemprop="ingredients">1-2 guindillas frescas (según el grado de picor y el gusto)</span></li><li><span itemprop="ingredients"> pimienta negra recién molida</span></li><li><span itemprop="ingredients">4 c/s de queso Grana Padano rallado</span></li><li><span itemprop="ingredients"> aceite de oliva virgen extra</span></li><li><span itemprop="ingredients"> sal.</span></li></ul>
<h3>Elaboración</h3>
<p>Limpia las coles de Bruselas, corta por la base y retira las hojas externas, lávalas y escúrrelas bien. Si son grandecitas córtalas por la mitad, si son pequeñas puedes dejarlas enteras, y claro, puedes poner algunas enteras combinadas con las más grandes cortadas.</p>
<p><a href="https://gastronomiaycia.republica.com/fotos/recetas/colbruselas_pinon_guindill1.jpg" title="Coles de Bruselas con piñones, ajo y guindilla"><img src="https://gastronomiaycia.republica.com/wp-content/photos/colbruselas_pinon_guindill1.jpg" class="centro no-print" alt="Coles de Bruselas con piñones, ajo y guindilla" width="680" height="456"></a></p>
<p>Pon una olla con agua al fuego, añade una cucharada de sal. Cuando empiece a hervir incorpora las coles de Bruselas, cuécelas unos cinco minutos y retíralas del fuego, escurre las coles y reserva.</p>
<p>Ahora pon una sartén o parrilla al fuego, calienta un poco de aceite de oliva para saltear las coles de Bruselas, ponlas en primer lugar con el corte hacia abajo para que se doren, salpiméntalas al gusto, y prepara los ajos pelados y laminados, la guindilla despepitada y cortada en aros y los piñones.</p>
<p>Incorpora estos ingredientes a la parrilla con las coles y dora todos los ingredientes salteando o moviendo de vez en cuando. En el último momento espolvorea el queso rallado y si lo deseas, un hilo de aceite de oliva virgen extra.</p>
<h3>Emplatado</h3>
<p>Sirve enseguida las <strong>Coles de Bruselas con piñones</strong>, ajo y guindilla, una delicia, tus comensales querrán repetir.</p>
<p><em>Abreviaturas</em><br>
c/s = Cuchara sopera<br>
c/p = Cuchara de postre<br>
c/c = Cuchara de café</p>
</div></div><!-- post-end -->
							
<div class="entry-socialshare"><div class="es-row"><a href="https://twitter.com/intent/tweet?text=Coles+de+Bruselas+con+pi%C3%B1ones%2C+ajo+y+guindilla&amp;url=https%3A%2F%2Fgastronomiaycia.republica.com%2F2010%2F10%2F26%2Fcoles-de-bruselas-con-pinones-ajo-y-guindilla%2F&amp;via=Gastronomiaycia" target="_blank" onclick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" class="ico-twitter">Twitter</a><a href="https://www.pinterest.es/pin/create/bookmarklet/?url=https%3A%2F%2Fgastronomiaycia.republica.com%2F2010%2F10%2F26%2Fcoles-de-bruselas-con-pinones-ajo-y-guindilla%2F&amp;media=https%3A%2F%2Fgastronomiaycia.republica.com%2Fwp-content%2Fuploads%2F2013%2F09%2Fcolbruselas_pinon_guindilla.jpg&amp;h=361&amp;w=500&amp;description=Coles de Bruselas con piñones, ajo y guindilla" target="_blank" onclick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" class="ico-pinterest">Pinterest</a><a href="https://www.facebook.com/share.php?u=https://gastronomiaycia.republica.com/2010/10/26/coles-de-bruselas-con-pinones-ajo-y-guindilla/" target="_blank" onclick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" class="ico-facebook">Facebook</a><a href="https://share.flipboard.com/bookmarklet/popout?v=2&amp;title=Coles+de+Bruselas+con+pi%C3%B1ones%2C+ajo+y+guindilla&amp;url=https%3A%2F%2Fgastronomiaycia.republica.com%2F2010%2F10%2F26%2Fcoles-de-bruselas-con-pinones-ajo-y-guindilla%2F" target="_blank" onclick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" class="ico-flipboard">Flipboard</a></div></div>
					</div>
		
		<!--
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<header class="align-center">
								<p>maecenas sapien feugiat ex purus</p>
								<h2>Lorem ipsum dolor</h2>
							</header>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at dignissim augue, in iaculis neque. Etiam bibendum felis ac vulputate pellentesque. Cras non blandit quam. Nunc porta, est non posuere sagittis, neque nunc pellentesque diam, a iaculis lacus urna vitae purus. In non dui vel est tempor faucibus. Aliquam erat volutpat. Quisque vel est vitae nibh laoreet auctor. In nec libero dui. Nulla ullamcorper, dolor nec accumsan viverra, libero eros rutrum metus, vel lacinia magna odio non nunc. Praesent semper felis eu rhoncus aliquam. Donec at quam ac libero vestibulum pretium. Nunc faucibus vel arcu in malesuada. Aenean at velit odio. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas commodo erat eget molestie sollicitudin. Donec imperdiet, ex sed blandit dictum, ipsum metus ultrices arcu, vitae euismod nisl sapien vitae tortor.</p>

							<p>Vivamus nec odio ac ligula congue feugiat at vitae leo. Aenean sem justo, finibus sed dui eu, accumsan facilisis dolor. Fusce quis dui eget odio iaculis aliquam vel sed velit. Nulla pellentesque posuere semper. Nulla eu sagittis lorem, a auctor nulla. Sed ac condimentum orci, ac varius ante. Nunc blandit quam sit amet sollicitudin sodales.</p>

							<p>Vivamus ultricies mollis mauris quis molestie. Quisque eu mi velit. In et cursus nibh. Donec facilisis, orci sed mollis hendrerit, nunc risus mattis odio, eget efficitur nisl orci a lectus. Aenean finibus neque convallis orci sollicitudin tincidunt. Vivamus lacinia facilisis diam, quis facilisis nisi luctus nec. Aliquam ac molestie enim, ut ultrices elit. Fusce laoreet vulputate risus in tincidunt. Sed commodo mollis maximus. Nullam varius laoreet nibh sit amet facilisis. Donec ac odio vehicula, consequat elit et, sodales justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam ac auctor mauris, in hendrerit libero. </p>
						</div>
					</div>
				</div>
			</section>
		-->
		<!-- Footer --> 
		
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>