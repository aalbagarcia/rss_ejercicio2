<!DOCTYPE HTML>
<!--
	Hielo by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Hielo by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="index.html">Hielo <span>by TEMPLATED</span></a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
<?php

require __DIR__ . '/vendor/autoload.php';
$smarty = New Smarty();

echo $smarty->display('menu.tpl');

?>

		<!-- One -->
			<section id="One" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>otro clásico de madrugada despues de la rumba</p>
						<h2>Arepa Pelua</h2>
					</header>
				</div>


			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2">
				<div class="inner">
					<div class="box">
						<div class="content">
							<header class="align-center">
								
								<h2>Ingredientes para Arepas Pelua</h2>
							</header>
<div style="float:left;width:40%">
<ul><li>6 ó 7 arepas de maíz (según la cantidad de comensales)</li><li>
    Queso amarillo rallado</li><li>
    1 kilo de Falda de res (skirt steak)</li><li>
    1 pimentón rojo (pimiento, ají morrón o chili morrón)</li><li>
    2 cebollas grandes</li><li>
    1 zanahoria</li><li>
    200 ml de cerveza</li><li>
    1/2 cucharada de comino molido</li><li>
    Pimentón español dulce o paprika</li><li>
    Salsa inglesa o salsa Worcestershire</li>
</ul>	</div>
                            <div style="float:right;width:60%;text-align:right">
                                <img src="http://www.arepasreceta.com/wp-content/uploads/2017/12/Arepa-Pelúa.jpg" style="width:90%" />
                            </div>
                            <div style="clear:both"></div>
														<header class="align-center">
								
								<h2>Preparación</h2>
							</header>
							<p>
							Se pica la carne en trozos, se agrega en una olla con abundante agua <strong>(que la carne quede abajo)</strong>. 
							Cocinar hasta que quede blanda. Inmediatamente la sacamos, escurrimos y se deja enfriar.
							
							Al estar fría la carne se toma un tenedor y se <strong>deshilacha</strong> con el mismo hasta sacar hebras de carne. 
							Colocar un sarten y agregar el aceite de oliva, esperar 5 segundos, echar la cebolla, pimiento,&nbsp;&nbsp; tomate, comino. 
							Cocinar mezclando todos los ingredientes mencionados, completar todo con la carne desmenuzada, echar la salsa inglesa y cilantro. 
							Dejar cocinar 3 minutos. Probar y si le falta sal agregar a su gusto.Rellenar  la arepa realizando a la misma un corte en el medio y 
							se abre tipo bolsa para colocar la mezcla (rellenar) con la carne realizada anteriormente. Y le ponemos el queso y queda <strong>lista para degustar</strong>
							
</p>
					</div>
				</div>
			</section>
		

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
				</div>
				<div class="copyright">
					&copy; Untitled. All rights reserved.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>